package com.pgs.mhallman.springdocker.repositroy;

import com.pgs.mhallman.springdocker.entity.PersonEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends PagingAndSortingRepository<PersonEntity, Long>{
}
