package com.pgs.mhallman.springdocker.entity;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name="person")
@Entity(name = "PERSON")
public class PersonEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PERSON")
	private long id;

	@Column(name = "FIRSTNAME")
	private String firstName;

	@Column(name = "LASTNAME")
	private String lastName;

	@Column(name = "USERNAME")
	private String userName;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "IS_ACTIVE")
	private boolean isActive;
}
