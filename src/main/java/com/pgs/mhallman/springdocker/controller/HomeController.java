package com.pgs.mhallman.springdocker.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/")
public class HomeController {

	@RequestMapping(method = RequestMethod.GET, value = "home")
	public String helloWorld() {
		return "Hey buddy!";
	}

}
