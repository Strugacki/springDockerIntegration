package com.pgs.mhallman.springdocker.controller;

import com.pgs.mhallman.springdocker.entity.PersonEntity;
import com.pgs.mhallman.springdocker.repositroy.PersonRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/person")
public class PersonController {

	private PersonRepository personRepository;

	public PersonController(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@RequestMapping(path = "/all", method = RequestMethod.GET)
	public ResponseEntity getAll() {
		return new ResponseEntity(personRepository.findAll(), HttpStatus.OK);
	}

}
