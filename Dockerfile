FROM openjdk:alpine
MAINTAINER Marcin Hallman
ADD target/springdocker-1.0-SNAPSHOT.jar springdocker-1.0-SNAPSHOT.jar
LABEL name="strugacki-springdocker"

EXPOSE 8090
ENTRYPOINT ["java", "-jar", "springdocker-1.0-SNAPSHOT.jar"]